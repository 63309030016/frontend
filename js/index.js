var day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
store,
more  = 9;


$(document).ready(function(){

    $.ajax({
        type: "GET",
        url: "example_data.json",
        success:function(data){
            str = "";
            storeIndex = 0;
            store = data;
            if(store.length > 0){
                storeList();
                
            }
            
        }
    })
});

function storeList(){
    let cnt = 0 , storeNum;
    storeNum = store.length;
    while(storeIndex < storeNum){
        
        var d = new Date();
        var dayIndex =  d.getDay();
        var today = day[dayIndex];
        dateStore = eval(store[storeIndex].operation_time);
        day_find = dateStore.find(item => item.day === today);
        let typetimemoring,typetimeafternoon; 
        if(day_find.time_open != 'closed'&& day_find.time_close != 'closed'){
            
            typetimemoring = 'AM';
            typetimeafternoon = 'PM';
            str += '<div id="divres" class="column block-res">';
            str += '<table style="width: 100%;">';
            str += '<thead id="tableres">';
            str += '<tr class="for-moblie"><img class="img-res for-moblie" src="'+store[storeIndex].profile_image_url+'"></tr>';
            str += '<tr>';
            str += '<th class="for-web" rowspan="2"><img class="img-res" src="'+store[storeIndex].profile_image_url+'"</th>';
            str += '<th class="pd-left-19 res-name" colspan="2">'+store[storeIndex].name+'</th>'
            str += '</tr>';
            str += '<tr>';
            str += '<th align="left"><img class="pd-left-19" src="img/Vector.png">&nbsp;'+day_find.time_open+'&nbsp;'+typetimemoring+'&nbsp;-&nbsp;'+day_find.time_close+'&nbsp;'+typetimeafternoon+'</th>';
            str += '<th style="text-align: right;"><img class="pd-left-100" src="img/blue_point.png">&nbsp;'+store[storeIndex].rating+'</th>';
            str += '</tr>';
            str += '</thead>';
            str += '<tbody>';
            str += '<tr>';
            str += '<td colspan="3" class="for-web"><img class="img-food1 img-size" src="'+store[storeIndex].images[0]+'"><img class="img-size" src="'+store[storeIndex].images[1]+'"><img class="img-food2 img-size" src="'+store[storeIndex].images[2]+'">';
            str += '</td>';
            str += '<td class="for-moblie" colspan="3"><img class="img-food1" src="'+store[storeIndex].images[0]+'"></td>';
            str += '</tr>';
            str += '</tbody>';
            str += '</table>';
            str += '</div>'
            cnt++;
        }else {
            typetimemoring = '';
            typetimeafternoon = '';
        }
        storeIndex++;
        
        // if(cnt >= more) break;
    }
    $("#listStore").html(str);
    var Numpage = Math.round(cnt / more);
    $('.block-res:gt('+(more-1)+')').hide();
    let html = "";
    html +="<a class='block-pagepre' href='javascript:void(0)'><img src='img/left.png'></a>"
    html += "<a class='block-page active' id='pageId' href='javascript:void(0)'>1</a>"
    for(i=2; i <= Numpage; i++){
       html += "<a class='block-page' id='pageId' href='javascript:void(0)'>"+i+"</a>"
    }
    html += "<a class='block-pagenext' href='javascript:void(0)'><img src='img/right.png'></a>"
    $("#buttonPage").append(html);
    $(".block-page").click(function(){
        if($(this).hasClass('active')){
            return false;
        }else{
            var currentpage = $(this).index();
            $(".block-page").removeClass('active');
            $(this).addClass('active');
            $('.block-res').hide();
            var grandTotal = more * currentpage;
            for (var i = grandTotal - more; i < grandTotal; i++) {
                $('.block-res:eq('+i+')').show(); // Show items from the new page that was selected
              }
        }
    })

    // $("#searchRes").on("keyup", function() {
    //     var value = $(this).val().toLowerCase();
    //     let s = $(".block-res").length;
    //     let z = document.getElementsByClassName('block-res');
        
    //         for(i=0;i<s;i++){
    //             $(".res-name:gt("+i+")").filter(function() {
    //             console.log(z[i]);
    //             console.log($(this).text().toLowerCase().indexOf(value));
    //             if($(this).text().toLowerCase().indexOf(value) > -1){
    //                 z[i].style.display = "none";
    //             }else{
    //                 z[i].style.display = "";
    //             }
    //             });
    //         }
    //         // $(".block-res").toggle($(this).text().toLowerCase().indexOf(value) > -1);
       
        
    //     console.log(s);
    // });
}